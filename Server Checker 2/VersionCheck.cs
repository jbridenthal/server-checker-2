﻿using System;
using System.Net;
using System.Reflection;

namespace Server_Checker_2
{
    internal class VersionCheck
    {
        private const string Url = @"http://pastebin.com/raw.php?i=AsfVKe32";
        public string Download;
        public Version Version = Assembly.GetEntryAssembly().GetName().Version;
        public string WebVersion;
        private bool _webError;


        private string GetWebStringResponse()
        {
            string webResponse;
            try
            {
                var wc = new WebClient();
                webResponse = wc.DownloadString(Url);
                _webError = false;
            }
            catch (Exception ex)
            {
                webResponse = ex.Message;
                _webError = true;
            }

            return webResponse;
        }

        public void GetValuesFromWeb()
        {
            var webResponse = GetWebStringResponse();
            if (_webError) return;
            var splitByTilde = webResponse.Split('~');
            WebVersion = splitByTilde[0].Trim();
            Download = splitByTilde[1].Trim();
        }


        public bool CheckVersion()
        {
            GetValuesFromWeb();
            return Version.ToString().Equals(WebVersion);
        }
    }
}