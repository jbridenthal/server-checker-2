﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Media;
using System.Threading;
using System.Windows.Forms;
using Timer = System.Windows.Forms.Timer;

namespace Server_Checker_2
{
    public partial class FrmMain : Form
    {
        private readonly Dictionary<string, TabPage> _tabPageNames = new Dictionary<string, TabPage>();
        private readonly Timer _timer = new Timer();
        private readonly VersionCheck _version = new VersionCheck();
        private ServerData _serverData;
        private int _tickNumber = 30;
        private WebData _webData;

        public FrmMain()
        {
            InitializeComponent();
        }

        private void FrmMainLoad(object sender, EventArgs e)
        {
            lblVersion.Text = String.Format("Version:\r{0}", _version.Version);
            _version.GetValuesFromWeb();
            if (!_version.CheckVersion())
            {
                linkDownloadClick.Visible = true;
                linkLabelReport.Visible = false;
            }
            notifyObject.ContextMenu = cmNotifyIconMenu;

            Text = String.Format("SFT Server Check v{0} by Jyncs", _version.Version);

            tbcServers.Appearance = TabAppearance.FlatButtons;
            _webData = new WebData();
            var serversInfo = _webData.GetValuesWebResponse();
            foreach (
                var serverSplit in
                    serversInfo.Select(s => s.Split(Convert.ToChar(";")))
                               .Where(serverSplit => !serverSplit[0].Equals("")))
            {
                _serverData = new ServerData(serverSplit);
                var tabControlItems = new TabControlItems();
                var tab = tabControlItems.CreateNewTab(_serverData);
                _tabPageNames.Add(tab.Text, tab);
                tbcServers.Controls.Add(tab);
            }
            _timer.Tick += TimerTick;
            progressBar.Minimum = 0;
            progressBar.Maximum = _tickNumber;
            progressBar.Step = 1;
            _timer.Interval = (1000)*(1);
            _timer.Enabled = true;
            _timer.Start();
        }

        private void TimerTick(object sender, EventArgs e)
        {
            if (_tickNumber == 0)
            {
                _tickNumber = 30;
                progressBar.Value = 0;
                RefreshData();
            }
            else
            {
                _tickNumber -= 1;
            }
            progressBar.PerformStep();
        }

        private void RefreshData()
        {
            _webData = new WebData();
            var serversInfo = _webData.GetValuesWebResponse();
            var message = "";
            foreach (
                var serverSplit in
                    serversInfo.Select(s => s.Split(Convert.ToChar(";")))
                               .Where(serverSplit => !serverSplit[0].Equals("")))
            {
                _serverData = new ServerData(serverSplit);
                if (!_tabPageNames.Keys.Contains(_serverData.ServerName.Trim())) continue;
                var tab = _tabPageNames[_serverData.ServerName.Trim()];
                tab.Controls["lblStaffTotal"].Text = _serverData.Staff;
                tab.Controls["lblPlayersTotal"].Text = _serverData.Players;
                var ckbx = (CheckBox) tab.Controls["chkServer"];
                if (ckbx.Checked & _serverData.Ratio < .1)
                {
                    message += String.Format("{0}[{1}]   \tPlayers:{2}\tStaff: {3}", Environment.NewLine,
                                             _serverData.ServerName.Trim(), _serverData.Players,
                                             _serverData.Staff);
                }
            }
            if (!message.Equals(""))
            {
                NotificationBalloon(message);
            }
        }

        private void NotificationBalloon(string message)
        {
            notifyObject.ShowBalloonTip(800, "SFT Server Alert", message, ToolTipIcon.Error);
            for (var i = 0; i < 6; i++)
            {
                Thread.Sleep(250);
                SystemSounds.Asterisk.Play();
            }
        }

        private void FrmMainResize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                Hide();
            }
            notifyObject.Visible = true;
        }

        private void NotifyObjectClick(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
            Show();
            WindowState = FormWindowState.Normal;
        }

        private void BtnRefreshClick(object sender, EventArgs e)
        {
            RefreshData();
            _tickNumber = 30;
            progressBar.Value = 0;
            _timer.Stop();
            _timer.Start();
        }

        private void LinkDownloadClickLinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(_version.Download);
        }

        private void NotifyObjectMouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
            }
        }

        private void MenuItemCloseClick(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MenuItemCheckClick(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void LinkLabel1LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://bitbucket.org/jbridenthal/server-checker-2/issues?status=new&status=open");
        }

        private void LinkLabelSftLinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://www.superfuntime.org");
        }
    }
}