﻿using System.Drawing;
using System.Windows.Forms;
using Server_Checker_2.Properties;

namespace Server_Checker_2
{
    public class TabControlItems
    {
        public TabPage CreateNewTab(ServerData serverData)
        {
            var tab = new TabPage
                          {
                              Text = serverData.ServerName.Trim(),
                              Name = "tbp" + serverData.ServerName.Trim().Replace(" ", "")
                          };


            var lblPlayers = new Label
                                 {
                                     AutoSize = true,
                                     Location = new Point(17, 15),
                                     Name = "lblPlayers",
                                     Size = new Size(44, 13),
                                     TabIndex = 0,
                                     Text = Resources.TabControlItems_CreateNewTab_Players_
                                 };

            var lblStaff = new Label
                               {
                                   AutoSize = true,
                                   Location = new Point(17, 38),
                                   Name = "lblStaff",
                                   Size = new Size(32, 13),
                                   TabIndex = 1,
                                   Text = Resources.TabControlItems_CreateNewTab_Staff_
                               };

            var lblPlayersTotal = new Label
                                      {
                                          AutoSize = false,
                                          Location = new Point(104, 15),
                                          Name = "lblPlayersTotal",
                                          Size = new Size(35, 13),
                                          TabIndex = 2,
                                          Text = serverData.Players,
                                          TextAlign = ContentAlignment.MiddleRight
                                      };

            var lblStaffTotal = new Label
                                    {
                                        AutoSize = false,
                                        Location = new Point(104, 34),
                                        Name = "lblStaffTotal",
                                        Size = new Size(35, 13),
                                        TabIndex = 3,
                                        Text = serverData.Staff,
                                        TextAlign = ContentAlignment.MiddleRight
                                    };

            var chkServer = new CheckBox
                                {
                                    AutoSize = true,
                                    Location = new Point(20, 57),
                                    Name = "chkServer",
                                    Size = new Size(86, 17),
                                    TabIndex = 4,
                                    Text = Resources.TabControlItems_CreateNewTab_Server_Alerts,
                                    UseVisualStyleBackColor = true
                                };

            tab.Controls.Add(lblPlayers);
            tab.Controls.Add(lblPlayersTotal);
            tab.Controls.Add(lblStaff);
            tab.Controls.Add(lblStaffTotal);
            tab.Controls.Add(chkServer);

            return tab;
        }
    }
}