﻿using System;
using System.Collections.Generic;

namespace Server_Checker_2
{
    public class ServerData
    {
        public string ServerName;
        public string Players;
        public string Staff;
        public float Ratio;
        private readonly SharedTools _st = new SharedTools();

        public ServerData(IList<string> serverInfo)
        {
            ServerName = serverInfo[0].Substring(serverInfo[0].IndexOf(":", StringComparison.Ordinal) + 1);
            Players = serverInfo[1].Substring(serverInfo[1].IndexOf(":", StringComparison.Ordinal) + 1);
            Staff = serverInfo[2].Substring(serverInfo[2].IndexOf(":", StringComparison.Ordinal) + 1);
            Ratio = CalculateRatio((int) Convert.ToInt64(Staff), (int) Convert.ToInt64(Players));
        }

        private float CalculateRatio(int staff, int players)
        {
            double ratio;
            if (players == 0)
                ratio = 1.0;
            else
            {
                ratio = _st.Isnull(staff, 0)/_st.Isnull(players, 1);
            }
            return (float) ratio;
        }
    }
}