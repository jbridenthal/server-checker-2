﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server_Checker_2
{
    internal class SharedTools
    {
        public float Isnull(int checkValue, int nullValue)
        {
            var newValue = checkValue;
            return newValue == 0 ? nullValue : newValue;
        }
    }
}