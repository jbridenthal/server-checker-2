﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server_Checker_2
{
    internal class WebData
    {
        private const string Url = @"http://www.superfuntime.org/feeds/SFTserverchecker.php";
        private bool _webError;

        private string GetWebStringResponse()
        {
            string webResponse;
            try
            {
                var wc = new System.Net.WebClient();
                webResponse = wc.DownloadString(Url);
                _webError = false;
            }
            catch (Exception ex)
            {
                webResponse = ex.Message;
                _webError = true;
            }

            return webResponse;
        }

        public string[] GetValuesWebResponse()
        {
            var splitByDoubleSemiColon = new string[] {};
            if (!_webError)
            {
                var webResponse = GetWebStringResponse();
                splitByDoubleSemiColon = webResponse.Split(new[] {";;"}, StringSplitOptions.None);
            }
            return splitByDoubleSemiColon;
        }
    }
}