﻿namespace Server_Checker_2
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.tbcServers = new System.Windows.Forms.TabControl();
            this.notifyObject = new System.Windows.Forms.NotifyIcon(this.components);
            this.btnRefresh = new System.Windows.Forms.Button();
            this.lblVersion = new System.Windows.Forms.Label();
            this.linkDownloadClick = new System.Windows.Forms.LinkLabel();
            this.cmNotifyIconMenu = new System.Windows.Forms.ContextMenu();
            this.menuItemCheck = new System.Windows.Forms.MenuItem();
            this.menuItemClose = new System.Windows.Forms.MenuItem();
            this.linkLabelReport = new System.Windows.Forms.LinkLabel();
            this.linkLabelSFT = new System.Windows.Forms.LinkLabel();
            this.progressBar = new Server_Checker_2.VerticalProgressBar();
            this.SuspendLayout();
            // 
            // tbcServers
            // 
            this.tbcServers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbcServers.HotTrack = true;
            this.tbcServers.Location = new System.Drawing.Point(10, 12);
            this.tbcServers.Name = "tbcServers";
            this.tbcServers.SelectedIndex = 0;
            this.tbcServers.Size = new System.Drawing.Size(496, 116);
            this.tbcServers.TabIndex = 0;
            // 
            // notifyObject
            // 
            this.notifyObject.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyObject.Icon")));
            this.notifyObject.Text = "SFT Server Check";
            this.notifyObject.Visible = true;
            this.notifyObject.DoubleClick += new System.EventHandler(this.NotifyObjectClick);
            this.notifyObject.MouseClick += new System.Windows.Forms.MouseEventHandler(this.NotifyObjectMouseClick);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.Location = new System.Drawing.Point(544, 12);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(98, 23);
            this.btnRefresh.TabIndex = 4;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.BtnRefreshClick);
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVersion.Location = new System.Drawing.Point(544, 38);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(98, 39);
            this.lblVersion.TabIndex = 5;
            this.lblVersion.Text = "Version:";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // linkDownloadClick
            // 
            this.linkDownloadClick.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.linkDownloadClick.Location = new System.Drawing.Point(547, 72);
            this.linkDownloadClick.Name = "linkDownloadClick";
            this.linkDownloadClick.Size = new System.Drawing.Size(95, 55);
            this.linkDownloadClick.TabIndex = 6;
            this.linkDownloadClick.TabStop = true;
            this.linkDownloadClick.Text = "New Version\r\nAvailable for\r\nDownload\r\nClick Here";
            this.linkDownloadClick.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.linkDownloadClick.Visible = false;
            this.linkDownloadClick.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkDownloadClickLinkClicked);
            // 
            // cmNotifyIconMenu
            // 
            this.cmNotifyIconMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemCheck,
            this.menuItemClose});
            // 
            // menuItemCheck
            // 
            this.menuItemCheck.Index = 0;
            this.menuItemCheck.ShowShortcut = false;
            this.menuItemCheck.Text = "Check Servers";
            this.menuItemCheck.Click += new System.EventHandler(this.MenuItemCheckClick);
            // 
            // menuItemClose
            // 
            this.menuItemClose.Index = 1;
            this.menuItemClose.Text = "Close";
            this.menuItemClose.Click += new System.EventHandler(this.MenuItemCloseClick);
            // 
            // linkLabelReport
            // 
            this.linkLabelReport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabelReport.AutoSize = true;
            this.linkLabelReport.Location = new System.Drawing.Point(563, 72);
            this.linkLabelReport.Name = "linkLabelReport";
            this.linkLabelReport.Size = new System.Drawing.Size(59, 52);
            this.linkLabelReport.TabIndex = 7;
            this.linkLabelReport.TabStop = true;
            this.linkLabelReport.Text = "Click Here \r\nto report\r\nbugs or\r\nideas!";
            this.linkLabelReport.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.linkLabelReport.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabel1LinkClicked);
            // 
            // linkLabelSFT
            // 
            this.linkLabelSFT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabelSFT.AutoSize = true;
            this.linkLabelSFT.Location = new System.Drawing.Point(198, 131);
            this.linkLabelSFT.Name = "linkLabelSFT";
            this.linkLabelSFT.Size = new System.Drawing.Size(121, 13);
            this.linkLabelSFT.TabIndex = 8;
            this.linkLabelSFT.TabStop = true;
            this.linkLabelSFT.Text = "www.SuperFunTime.org";
            this.linkLabelSFT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabelSftLinkClicked);
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(512, 11);
            this.progressBar.Maximum = 30;
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(25, 116);
            this.progressBar.Step = 1;
            this.progressBar.TabIndex = 3;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 161);
            this.Controls.Add(this.linkLabelSFT);
            this.Controls.Add(this.linkLabelReport);
            this.Controls.Add(this.linkDownloadClick);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.tbcServers);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMain";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.FrmMainLoad);
            this.Resize += new System.EventHandler(this.FrmMainResize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TabControl tbcServers;
        private System.Windows.Forms.NotifyIcon notifyObject;
        private VerticalProgressBar progressBar;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.LinkLabel linkDownloadClick;
        private System.Windows.Forms.ContextMenu cmNotifyIconMenu;
        private System.Windows.Forms.MenuItem menuItemClose;
        private System.Windows.Forms.MenuItem menuItemCheck;
        private System.Windows.Forms.LinkLabel linkLabelReport;
        private System.Windows.Forms.LinkLabel linkLabelSFT;
    }
}

