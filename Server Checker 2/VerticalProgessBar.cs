﻿using System.Windows.Forms;

namespace Server_Checker_2
{
    public class VerticalProgressBar : ProgressBar
    {
        protected override CreateParams CreateParams
        {
            get
            {
                var cp = base.CreateParams;
                cp.Style |= 0x04;
                return cp;
            }
        }
    }
}